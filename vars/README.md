# layered-automation
Ansible Automates March 25th 2022 [Register to view session](https://events.redhat.com/profile/form/index.cfm?PKformID=0x516307abcd&sc_cid=7013a0000026LhkAAE&utm_source=bambu&utm_medium=social)

# Overview
This repo provides example code and additional details to suppliment the Ansible Automates 20222 session "Automating Layered Networks with Ansible Automation Platform 2.x". The goal here is to provide additional details to explain the session demo. Due to brevety, many aspects of the underpining of the "code" were ommited. For your own purposes, it's possible to adjust inventory, vendor, OS types to fit your own staging environment. Passwords should be encrypted/vault but for my lab (airgap) they were left clear. This code is  provided "as is" and not recommended or supported for production use. 

# Demo
The demo was built using IOS-XE and IOS-XR virtual routers. Other models could be substituted. As for Cisco, the Cisco Modleing Labs (CML 2.x) is the easiest method/platform to provide a simular topology.

[Cisco Modeling Labs](https://www.cisco.com/c/en/us/products/cloud-systems-management/modeling-labs/index.html)

The Redhat Ansible Automation Platform 2.x trial is available below:
[AAP2.x Trial](https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it)


# Topology

## L3VPN  MPLS
<img src="pics/topology.png"  width="820" height="420">


## Management
| Host      | OS     |Mgmnt IP      |
| --------- | ------ | ------------ |
|core-rtr01 |IOS XR  |192.168.0.173 |
|core-rtr02 |IOS XR  |192.168.0.174 |
|dist-rtr01 |IOS XE  |192.168.0.165 |
|dist-rtr02 |IOS XE  |192.168.0.176 |

# Router Configs
**base configs:** base_configs/
**solution conigs:** solution_configs/ 

# Ansible Controller (as code)
The following playbook and vars can be manipulated as an alternative to prepare the GUI components of the Demo.
- vars/var.yml 
- controller.yml

This playbook will create the following controller contructs:
- credentials
- inventory
- inventory_source
- project
- job templates
- workflow templates
- webhooks

## EE or Virtenv
If using CLI with ansible-playbook or ansible-navigator to run the controller.yml, please insure to have either ansible.cotroller or awx.awx collection available. This could be done manually or using a requirments.yml file depending on your preference.

### Virtualenv vs Execution environment
[Blog](https://www.ansible.com/blog/migrating-from-python-virtual-environments-to-automation-execution-environments-in-ansible-automation-platform-2)


## Run playbook
~~~
ansible-playbook controller.yml
~~~

# Demo REPO contents
~~~
├── ansible.cfg
├── base_configs
│   ├── core-rtr01
│   ├── core-rtr02
│   ├── dist-rtr01
│   └── dist-rtr02
├── controller.yml
├── criteria
│   ├── ios_ipsla_criteria.json
│   ├── ios_show_bgp_criteria.json
│   └── iosxr_show_bgp_criteria.json
├── hosts.yml
├── playbooks
│   ├── core-base-rollback.yml
│   ├── core-bgp-validate.yml
│   ├── core-bgp.yml
│   ├── core-vpn-rollback.yml
│   ├── core-vpn.yml
│   ├── dist-bgp-validate.yml
│   ├── dist-vpn-rollback.yml
│   ├── dist-vpn.yml
│   ├── ipsla_config.yml
│   └── ipsla_results.yml
├── README.md
├── roles
│   ├── core-base-rollback
│   │   └── tasks
│   │       ├── iosxr.yml
│   │       └── main.yml
│   ├── core-bgp
│   │   └── tasks
│   │       ├── iosxr.yml
│   │       └── main.yml
│   ├── core-vpn
│   │   └── tasks
│   │       ├── iosxr.yml
│   │       └── main.yml
│   ├── core-vpn-rollback
│   │   └── tasks
│   │       ├── iosxr.yml
│   │       └── main.yml
│   ├── dist-vpn
│   │   └── tasks
│   │       ├── ios.yml
│   │       └── main.yml
│   ├── dist-vpn-rollback
│   │   └── tasks
│   │       ├── ios.yml
│   │       └── main.yml
│   ├── ipsla_core
│   │   └── tasks
│   │       ├── iosxr.yml
│   │       └── main.yml
│   └── ipsla_dist
│       └── tasks
│           ├── ios.yml
│           └── main.yml
├── solution_configs
│   ├── core-rtr01
│   ├── core-rtr02
│   ├── dist-rtr01
│   └── dist-rtr02
├── templates
│   ├── ce-ipsla.j2
│   ├── cevpn.j2
│   ├── cevpn-rollback.j2
│   ├── pe-ipsla.j2
│   ├── pevpn.j2
│   └── pevpn-rollback.j2
└── vars
    └── vars.yml
~~~

# Ansible Controller Configurations

## Inventory and Variables

**L3vpn-MPLS** This inventory provides host_vars and Group_vars for the demo
- Inventory Source: layered_network
- Updated from hosts.yml using the gitlab staging branch 


## Workflow Job Template
**L3VPN-MPLS:** This workflow configures each layer of the L3VPN MPLS and provides validations and rollbacks.
- Webhook: Integrated to Gitlab staging branch as a push trigger
  - Push Trigger: this type of trigger is invoked any time a file in the SCM staging branch is commited and pushed with GIT
  - The webhook will start a L3VPN-MPLS workflow in the Ansible Controller 
- Project: Updates the playbooks from SCM (gitlab) staging branch for the BGP-BASE-MPLS project

Please note below, some playbooks/roles utilize resouce modules instead of jinja2 templates. Resource modules are 
preferred but keep in mind templates are necessary for gaps in device configuration. Over time resource modules continue to expand coverage.

### Job Templates
| Job Templates          | Playbooks                        | Roles                  | Templates        | OS       |
| -----------------------| -------------------------------- | -----------------------| -----------------|----------|
|L3VPN-CORE-BGP          |playbooks/core-bgp.yml            |roles/core-bgp          |                  |iosxr.yml |
|L3VPN-CORE-BGP-ROLLBACK |playbooks/core-base-rollback.yml  |roles/core-base-rollback|                  |iosxr.yml |
|L3VPN-PE-BGP-Validate   |playbooks/core-bgp-validate.yml   |                        |                  |          |
|L3VPN-CORE-VPN          |playbooks/core-vpn.yml            |roles/core-vpn          |pevpn.j2          |iosxr.yml |
|L3VPN-DIST-VPN          |playbooks/dist-vpn.yml            |roles/dist-vpn          |cevpn.j2          |ios.yml   |
|L3VPN-DIST-VPN-ROLLBACK |dist-vpn-rollback.yml             |roles/dist-vpn-rollback |cevpn-rollback.j2 |ios.yml   |
|L3VPN-DIST-CORE-ROLLBACK|playbooks/core-vpn-rollback.yml   |roles/core-vpn-rollback |pevpn-rollback.j2 |iosxr.yml |
|L3VPN-CE-BGP-Validate   |playbooks/dist-bgp-validate.yml   |                        |                  |          |
|L3VPN-IPSLA-CONFIG      |playbooks/ipsla_config.yml        |roles/ipsla_dist        | ce-ipsla.j2      |ios.yml   |  
|L3VPN-IPSLA-RESULTS     |playbooks/ipsla_results.yml       |                        |                  |          |

## L3VPN-MPLS
<img src="pics/l3vpnmpls.png"  width="1220" height="320">


## Workflow Job Template

**L3VPN-MPLS-Cleanup** Used to reset the demo environemnt by rolling back the L3VPN configurations

### Job Templates
| Job Templates          | Playbooks                        | Roles                  | Templates        | OS       |
| -----------------------| -------------------------------- | -----------------------| -----------------|----------|
|L3VPN-CORE-BGP-ROLLBACK |playbooks/core-base-rollback.yml  |roles/core-base-rollback|                  |iosxr.yml |
|L3VPN-DIST-VPN-ROLLBACK |dist-vpn-rollback.yml             |roles/dist-vpn-rollback |cevpn-rollback.j2 |ios.yml   |
|L3VPN-DIST-CORE-ROLLBACK|playbooks/core-vpn-rollback.yml   |roles/core-vpn-rollback |pevpn-rollback.j2 |iosxr.yml |

## L3VPN-MPLS-Cleanup
<img src="pics/cleanup.png"  width="820" height="420">


# Validations
A key take away from this demo is the abiltiy to create a workflow/pipeline with pre/post validation checks. Validations of operational state are provided with the ansible.utils.validation module. 

| Job Templates        | Playbooks                      | Criteria                            | Remarks                 | 
| ---------------------| -------------------------------| ------------------------------------|-------------------------|
|L3VPN-PE-BGP-Validate |playbooks/core-bgp-validate.yml |criteria/iosxr_show_bgp_criteria.json|established BGP neighbors|             
|L3VPN-CE-BGP-Validate |playbooks/dist-bgp-validate.yml |criteria/ios_show_bgp_criteria.json  |established BGP neighbors|
|L3VPN-IPSLA-RESULTS   |playbooks/ipsla_results.yml     |criteria/ios_ipsla_criteria.json     |end-to-end ping

### Example Criteria
~~~
{
"type": "object",
"patternProperties": {
         "^.*": {
                 "type": "object",
                 "properties": {
                     "session_state":  {
                           "type":"string",
                           "pattern": "established" }}}}} **<==== Established BGP Neighbors between PE Core routers**
~~~
### Example playbook 
~~~
tasks:
  - name: Fetch bgp state and parse with pyats
    ansible.utils.cli_parse:
      command: show bgp neighbors 
      parser:
        name: ansible.netcommon.pyats
    register: iosxr_pyats_show_bgp_neighbors <===Data from "show bgp neihbors" command

  - name: validate bgp neighbor for established state
    ansible.utils.validate:
      data: "{{ iosxr_pyats_show_bgp_neighbors ['parsed']['instance']['all']['vrf']['default']['neighbor']}}"**<== Path to session state**
      criteria:
        - "{{ lookup('file','../criteria/iosxr_show_bgp_criteria.json') | from_json }}"
      engine: ansible.utils.jsonschema
    #ignore_errors: true
    register: result
~~~
### Example Output
<img src="pics/validate.png"  width="820" height="420">


# Next Steps
Merge your validated staging branch with the main branch
<img src="pics/merge.png"  width="820" height="420">






 
  
 
  

  
  
  
  








